package com.example.game.services;

import java.util.List;
import java.util.Optional;

import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;

public interface GenreService {

	public Genre addGenre(Genre genre);	
	public Optional<Genre> getGenre(String genre);
	public List<GenreResponse> getAll(); //Devuelve una lista de todos los generos
	public void deleteAll();
}
