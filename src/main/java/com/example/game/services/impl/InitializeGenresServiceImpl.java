package com.example.game.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.game.convert.GenreEnumToGenreConverter;
import com.example.game.convert.GenreResponseToGenre;
import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;
import com.example.game.enums.GenreEnum;
import com.example.game.services.GenreService;
import com.example.game.services.InitializeGenresService;

@Service
public class InitializeGenresServiceImpl implements InitializeGenresService {

	@Autowired
	GenreService genreService;
	
	@PostConstruct
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		List<GenreResponse> actualGenres = genreService.getAll();
		
		GenreResponseToGenre genreResponseToGenre = new GenreResponseToGenre();
		List<Genre> actualGenreList = new ArrayList<>();
		for (int i = 0; i < actualGenres.size(); i++) {
			actualGenreList.add(genreResponseToGenre.convert(actualGenres.get(i)));
		}
		
		GenreEnum[] genreEnums = GenreEnum.values();
		GenreEnumToGenreConverter genreEnumToGenre = new GenreEnumToGenreConverter();
		List<Genre> genreList = new ArrayList<>();
		for (int i = 0; i < genreEnums.length; i++) {
			genreList.add(genreEnumToGenre.convert(GenreEnum.values()[i]));
		}
		
		genreList.forEach((genre) -> {
			if(!actualGenreList.contains(genre)) {
				genreService.addGenre(genre);
			}
		});
	}

}
