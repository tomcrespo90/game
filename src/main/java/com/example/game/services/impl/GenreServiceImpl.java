package com.example.game.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.example.game.convert.GenreToGenreResponseConverter;
import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;
import com.example.game.repository.GenreRepository;
import com.example.game.services.GenreService;

@Service
public class GenreServiceImpl implements GenreService{
	
	@Autowired
	private GenreRepository genreRepo;
	
	@Autowired
	private ConversionService converter;

	@Override
	public Genre addGenre(Genre genre) {
		Genre addGenre = genreRepo.save(genre);
		return addGenre;
	}

	@Override
	public Optional<Genre> getGenre(String genre) {
		// TODO Auto-generated method stub
		return genreRepo.findByName(genre);
	} 

	@Override
	public List<GenreResponse> getAll() {
		List<Genre> genreList = genreRepo.findAll();
		GenreToGenreResponseConverter genreToGenreResponse = new GenreToGenreResponseConverter();
		return genreList.stream().map(gr -> genreToGenreResponse.convert(gr)).collect(Collectors.toList());
	}
	
	@Override
	public void deleteAll() {
		genreRepo.deleteAll();
	}

}
