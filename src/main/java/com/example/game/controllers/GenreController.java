package com.example.game.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.game.dtos.request.GameRequest;
import com.example.game.dtos.request.GenreRequest;
import com.example.game.dtos.response.GameResponse;
import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;
import com.example.game.services.GameService;
import com.example.game.services.GenreService;

@RestController
public class GenreController {

	@Autowired
	private GenreService genreService;

	@GetMapping("/genre")
	public ResponseEntity<Object> getGenre(@RequestParam("genre") String genreName, HttpServletRequest request) {
		return ResponseEntity.status(HttpStatus.OK).body(genreService.getGenre(genreName));
	}

	@GetMapping("/genreList")
	public ResponseEntity<Object> getGenre(HttpServletRequest request) {
		return ResponseEntity.status(HttpStatus.OK).body(genreService.getAll());
	}
}
