package com.example.game.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.UniqueElements;

import com.example.game.enums.GenreEnum;

import lombok.Data;

@Entity // Entidad que va a tener valores
@Table(name = "GENRES")
@Data
public class Genre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "GENRE")
	@Enumerated(EnumType.STRING)
	private GenreEnum genreName;

	@ManyToMany(mappedBy = "genres")
	private List<Game> games;

}
