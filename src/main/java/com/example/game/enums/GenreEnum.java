package com.example.game.enums;

public enum GenreEnum {

	ADVENTURE, ACTION, FIGHT, TERROR, STRATEGY, PUZZLE
}
