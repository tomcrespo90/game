package com.example.game.dtos.response;

import com.example.game.enums.GenreEnum;

import lombok.Data;

@Data
public class GenreResponse {

	private GenreEnum genreName;
	
}
