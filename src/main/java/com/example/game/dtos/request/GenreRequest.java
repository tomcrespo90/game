package com.example.game.dtos.request;

import com.example.game.enums.GenreEnum;

import lombok.Data;

@Data
public class GenreRequest {
	
	private GenreEnum genreName;

}
