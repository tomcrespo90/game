package com.example.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.example.game.entities.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{

	@Query("SELECT g FROM Genre g WHERE g.genreName = ?1")
	Optional<Genre> findByName(String name);
}
