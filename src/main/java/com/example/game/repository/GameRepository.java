package com.example.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.game.entities.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

	@Query("SELECT g FROM Game g WHERE g.title = ?1")
	Optional<Game> findByTitle(String title);

}
