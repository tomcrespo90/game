package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.entities.Genre;
import com.example.game.enums.GenreEnum;

public class GenreEnumToGenreConverter  implements Converter<GenreEnum, Genre>{

	@Override
	public Genre convert(GenreEnum genreEnum) {
		Genre genre = new Genre();
		genre.setGenreName(genreEnum);
		return genre;
	}
	
}