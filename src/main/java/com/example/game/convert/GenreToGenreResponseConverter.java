package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;

public class GenreToGenreResponseConverter implements Converter<Genre, GenreResponse>{

	@Override
	public GenreResponse convert(Genre genreName) {
		GenreResponse genreResponse = new GenreResponse();
		genreResponse.setGenreName(genreName.getGenreName());
		return genreResponse;
	}

}
