package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.response.GenreResponse;
import com.example.game.entities.Genre;

public class GenreResponseToGenre implements Converter<GenreResponse, Genre>{

	@Override
	public Genre convert(GenreResponse genreResponse) {
		Genre genre = new Genre();
		genre.setGenreName(genreResponse.getGenreName());
		return genre;
	}

}
